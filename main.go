// squid-api project main.go
package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
)

var commandFile = os.TempDir() + "/command"
var stateFile = os.TempDir() + "/state"
var squidConfigPath = os.Getenv("SQUID_ETC")
var squidBin = os.Getenv("SQUID_BIN")

func createCommand(contents string) error {
	err := ioutil.WriteFile(commandFile, []byte(contents), 0640)
	if err != nil {
		log.Fatal(err)
		return err
	} else {
		return nil
	}
}

func initialise() error {
	var createErr error
	if _, err := os.Stat(stateFile); err != nil {
		createErr = createCommand("disable")
	}
	return createErr
}

func loadAuthMap() map[string]string {
	jsonMap := make(map[string]string)
	b, err := ioutil.ReadFile("/etc/squid-api/auth.json")
	err = json.Unmarshal(b, &jsonMap)
	if err != nil {
		panic(err)
	}
	return jsonMap
}

func sigHupSquid() error {
	cmd := exec.Command(squidBin, "-k", "reconfigure")
	err := cmd.Run()
	if err != nil {
		log.Printf("Squid reconfigure error %s\n", err)
	}
	return err
}

func main() {
	initialise()
	router := gin.Default()
	router.LoadHTMLGlob("/etc/squid-api/templates/*")

	authorized := router.Group("/", gin.BasicAuth(loadAuthMap()))

	authorized.GET("/", func(c *gin.Context) {
		state, _ := ioutil.ReadFile(stateFile)
		command, _ := ioutil.ReadFile(commandFile)
		c.HTML(
			http.StatusOK,
			"status.html",
			gin.H{
				"state":   string(state),
				"command": string(command),
			},
		)

	})
	authorized.POST("/lock-disable", func(c *gin.Context) {
		createCommand("disable")
		c.Redirect(http.StatusMovedPermanently, "/")
	})

	authorized.POST("/lock-enable", func(c *gin.Context) {
		createCommand("enable")
		c.Redirect(http.StatusMovedPermanently, "/")
	})

	srv := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	// serve
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// run checks
	go func() {
		// control loop, run forever
		for range time.Tick(time.Duration(20) * time.Second) {
			command, err := ioutil.ReadFile(commandFile)
			if err != nil {
				continue
			}

			state := string(command)
			configFile := squidConfigPath + "/squid.conf." + state
			symlink := squidConfigPath + "/squid.conf"
			if _, err := os.Stat(symlink); err == nil {
				os.Remove(symlink)
			}
			err = os.Symlink(configFile, symlink)
			if err != nil {
				log.Fatalf("symlink error: %s\n ", err)
			}
			err = sigHupSquid()
			if err != nil {
				continue
			}
			os.Remove(commandFile)
			ioutil.WriteFile(stateFile, command, 0640)
		}
		log.Println("exiting command monitor")
	}()

	// graceful shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
